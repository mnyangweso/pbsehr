
CREATE EXTENSION pgcrypto;
CREATE EXTENSION "uuid-ossp";

﻿﻿DROP DATABASE pbsmike;

CREATE DATABASE pbsmike
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       CONNECTION LIMIT = -1;


ALTER database template1 SET datestyle TO 'SQL, DMY';
ALTER database postgres SET datestyle TO 'SQL, DMY';
ALTER DATABASE pbsmike SET DATESTYLE TO 'SQL, DMY';

\c pbsmike;

CREATE language 'plpgsql';

-----################################ Stored Procedure ################################---

---------------------------------------------------------------------------------------------------
-- Procedure for updated
---------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION pro_updated()
		RETURNS TRIGGER AS'
	BEGIN
		NEW.updated=now();
		RETURN NEW;
	END;
'LANGUAGE 'plpgsql';
---need to be executed by superuser


---------------################################ TRBAC ################################--------------

---------------------------------------------------------------------------------------------------
-- Table structure for trbac_module
---------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS trbac_module;

CREATE TABLE trbac_module (
  	id 				BIGSERIAL PRIMARY KEY NOT NULL,
    	name 				VARCHAR(120) UNIQUE NOT NULL ,
	installed 			BOOLEAN NOT NULL DEFAULT false,
	version 			NUMERIC(2,2) NOT NULL DEFAULT 0.00,
	root_only 			BOOLEAN NOT NULL DEFAULT false,

	isactive 			BOOLEAN NOT NULL DEFAULT false,
	created 			TIMESTAMP NOT NULL DEFAULT NOW(),
	createdby 			INTEGER NOT NULL DEFAULT 0,
	updated 			TIMESTAMP NOT NULL DEFAULT NOW(),
	updatedby 			INTEGER NOT NULL DEFAULT 0,
	description 			TEXT  
);

------------------trigger-----------------------------
CREATE TRIGGER tri_updated before UPDATE
ON  trbac_module FOR EACH ROW EXECUTE PROCEDURE
pro_updated();

---------------------------------------------------------------------------------------------------
-- Table structure for trbac_control
---------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS trbac_control;

CREATE TABLE trbac_control (
  	id 				BIGSERIAL PRIMARY KEY NOT NULL,
    	name 				VARCHAR(120) UNIQUE NOT NULL ,
	trbac_module_id 		INTEGER NOT NULL REFERENCES trbac_module(id),

	isactive 			BOOLEAN NOT NULL DEFAULT false,
	created 			TIMESTAMP NOT NULL DEFAULT NOW(),
	createdby 			INTEGER NOT NULL DEFAULT 0,
	updated 			TIMESTAMP NOT NULL DEFAULT NOW(),
	updatedby 			INTEGER NOT NULL DEFAULT 0,
	description 			TEXT
);

------------------trigger-----------------------------
CREATE TRIGGER tri_updated before UPDATE
ON  trbac_control FOR EACH ROW EXECUTE PROCEDURE
pro_updated();

---------------------------------------------------------------------------------------------------
-- Table structure for trbac_function
---------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS trbac_function;

CREATE TABLE trbac_function (
  	id 				BIGSERIAL PRIMARY KEY NOT NULL,
    	name 				VARCHAR(120) UNIQUE NOT NULL ,
	trbac_control_id 		INTEGER NOT NULL REFERENCES trbac_control(id),

	isactive 			BOOLEAN NOT NULL DEFAULT false,
	created 			TIMESTAMP NOT NULL DEFAULT NOW(),
	createdby 			INTEGER NOT NULL DEFAULT 0,
	updated 			TIMESTAMP NOT NULL DEFAULT NOW(),
	updatedby 			INTEGER NOT NULL DEFAULT 0,
	description 			TEXT
);

------------------trigger-----------------------------
CREATE TRIGGER tri_updated before UPDATE
ON  trbac_function FOR EACH ROW EXECUTE PROCEDURE
pro_updated();

---------------------------------------------------------------------------------------------------
-- Table structure for trbac_role
---------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS trbac_role;

CREATE TABLE trbac_role (
  	id 				BIGSERIAL PRIMARY KEY NOT NULL,
    	name 				VARCHAR(120) UNIQUE NOT NULL ,
	isactive 			BOOLEAN NOT NULL DEFAULT false,
	created 			TIMESTAMP NOT NULL DEFAULT NOW(),
	createdby 			INTEGER NOT NULL DEFAULT 0,
	updated 			TIMESTAMP NOT NULL DEFAULT NOW(),
	updatedby 			INTEGER NOT NULL DEFAULT 0,
	description 			TEXT
);

------------------trigger-----------------------------
CREATE TRIGGER tri_updated before UPDATE
ON  trbac_role FOR EACH ROW EXECUTE PROCEDURE
pro_updated();

-------add data
insert into trbac_role(name,id)values('superuser',0);
ALTER TABLE trbac_role ALTER COLUMN id SET DEFAULT nextval('trbac_role_id_seq'::regclass);



-----################################ Application ################################---
---------------------------------------------------------------------------------------------------
-- Table structure for app_serial
---------------------------------------------------------------------------------------------------

CREATE TABLE app_serial(
	id 				BIGSERIAL PRIMARY KEY NOT NULL,
    	name 				VARCHAR(120) UNIQUE NOT NULL ,
	
	increase 			INTEGER NOT NULL DEFAULT 1,
	min_val 			INTEGER NOT NULL DEFAULT 1,
	max_val 			INTEGER NOT NULL DEFAULT 9999999,
	start_val 			INTEGER NOT NULL DEFAULT 1,
	curr_val 			INTEGER NOT NULL DEFAULT 0,
	format_str			VARCHAR(60) NOT NULL,
	expires 			BOOLEAN NOT NULL DEFAULT false,
	isactive 			BOOLEAN NOT NULL DEFAULT false,
	created 			TIMESTAMP NOT NULL DEFAULT NOW(),
	createdby 			INTEGER NOT NULL DEFAULT 0,
	updated 			TIMESTAMP NOT NULL DEFAULT NOW(),
	updatedby 			INTEGER NOT NULL DEFAULT 0,
	description 			TEXT
 
);

------------------trigger-----------------------------
CREATE TRIGGER tri_updated before UPDATE
ON  app_serial FOR EACH ROW EXECUTE PROCEDURE
pro_updated();


---------------------------------------------------------------------------------------------------
-- Table structure for app_identifiers
---------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS app_identifiers;

CREATE TABLE app_identifiers (
  	id 				BIGSERIAL PRIMARY KEY NOT NULL,
    	name 				VARCHAR(120) UNIQUE NOT NULL ,
	patient_id 			INTEGER NOT NULL REFERENCES patient(id),
	guid 				UUID DEFAULT uuid_generate_v1mc(),
	national 			VARCHAR(120) UNIQUE,
	ccc 				VARCHAR(120) UNIQUE,
	tb 				VARCHAR(120) UNIQUE,
	nssf	 			VARCHAR(120) ,
	nhif	 			VARCHAR(120) ,
	isactive 			BOOLEAN NOT NULL DEFAULT false,
	created 			TIMESTAMP NOT NULL DEFAULT NOW(),
	createdby 			INTEGER NOT NULL DEFAULT 0,
	updated 			TIMESTAMP NOT NULL DEFAULT NOW(),
	updatedby 			INTEGER NOT NULL DEFAULT 0,
	description 			TEXT
);



------------------trigger-----------------------------
CREATE TRIGGER tri_updated before UPDATE
ON  trbac_role FOR EACH ROW EXECUTE PROCEDURE
pro_updated();

---------------------------------------------------------------------------------------------------
-- Table structure for codes
---------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS codes;

CREATE TABLE codes (
  	id 				BIGSERIAL PRIMARY KEY NOT NULL,
    	name 				VARCHAR(120) UNIQUE NOT NULL ,
	min_val				INTEGER,
	max_val				INTEGER,
	si_unit				INTEGER,
	isactive 			BOOLEAN NOT NULL DEFAULT false,
	created 			TIMESTAMP NOT NULL DEFAULT NOW(),
	createdby 			INTEGER NOT NULL DEFAULT 0,
	updated 			TIMESTAMP NOT NULL DEFAULT NOW(),
	updatedby 			INTEGER NOT NULL DEFAULT 0,
	description 			TEXT
);


-----################################ Patient ################################---
---------------------------------------------------------------------------------------------------
-- Table structure for patient
---------------------------------------------------------------------------------------------------
DROP IF EXISTS TABLE patient;

CREATE TABLE patient(
	id 				BIGSERIAL PRIMARY KEY NOT NULL,	
	salutation_type 		INTEGER REFERENCES codes(id),
	first_name 			VARCHAR(120) UNIQUE NOT NULL ,
	middle_name 			VARCHAR(120) UNIQUE NOT NULL ,
	last_name 			VARCHAR(120) UNIQUE NOT NULL ,
	nick_name 			VARCHAR(120) UNIQUE NOT NULL ,
	dob 				DATE NOT NULL ,
	marital_status_type 		INTEGER NOT NULL,
	sex_type 			INTEGER NOT NULL,
	registration_date 		DATE NOT NULL  DEFAULT CURRENT_DATE,
	deceased_date 			DATE,

	isactive 			BOOLEAN NOT NULL DEFAULT false,
	created 			TIMESTAMP NOT NULL DEFAULT NOW(),
	createdby 			INTEGER NOT NULL DEFAULT 0,
	updated 			TIMESTAMP NOT NULL DEFAULT NOW(),
	updatedby 			INTEGER NOT NULL DEFAULT 0,
	description 			TEXT
);

------------------trigger-----------------------------
CREATE TRIGGER tri_updated before UPDATE
ON  patient FOR EACH ROW EXECUTE PROCEDURE
pro_updated();


---------------------------------------------------------------------------------------------------
-- Table structure for patient_concent
---------------------------------------------------------------------------------------------------
DROP IF EXISTS TABLE patient_concent;

CREATE TABLE patient_concent(
	id 				BIGSERIAL PRIMARY KEY NOT NULL,
	
	allow_leave_msg	 		BOOLEAN NOT NULL DEFAULT false,
	allow_voice_msg	 		BOOLEAN NOT NULL DEFAULT false,
	allow_mail_msg	 		BOOLEAN NOT NULL DEFAULT false,
	allow_sms	 		BOOLEAN NOT NULL DEFAULT false,
	allow_email	 		BOOLEAN NOT NULL DEFAULT false,
	allow_immunization_registry  	BOOLEAN NOT NULL DEFAULT false,	
	allow_immunization_info_sharing	BOOLEAN NOT NULL DEFAULT false,
	allow_health_info_exchange 	BOOLEAN NOT NULL DEFAULT false,	
	allow_patient_web_portal 	BOOLEAN NOT NULL DEFAULT false,	

	isactive 			BOOLEAN NOT NULL DEFAULT false,
	created 			TIMESTAMP NOT NULL DEFAULT NOW(),
	createdby 			INTEGER NOT NULL DEFAULT 0,
	updated 			TIMESTAMP NOT NULL DEFAULT NOW(),
	updatedby 			INTEGER NOT NULL DEFAULT 0,
	description 			TEXT 
);

------------------trigger-----------------------------
CREATE TRIGGER tri_updated before UPDATE
ON  patient_concent FOR EACH ROW EXECUTE PROCEDURE
pro_updated();

---------------------------------------------------------------------------------------------------
-- Table structure for patient_vitals
---------------------------------------------------------------------------------------------------
DROP IF EXISTS TABLE patient_vitals;

CREATE TABLE patient_vitals(
	id 				BIGSERIAL PRIMARY KEY NOT NULL,
	weight 				DECIMAL(3,2), 
	height 				DECIMAL(3,2),
	bp_systolic 			INTEGER,
	bp_diastolic 			INTEGER,
	pulse 				INTEGER,
	respiration 			INTEGER,
	temp 				DECIMAL(3,2),
	temp_location 			INTEGER,
	oxygen_saturation 		INTEGER,
	head_circumference_cm 		INTEGER,
	waist_circumference_cm 		INTEGER,
	bmi 				INTEGER,
	bmi_status 			VARCHAR(120),
	other_notes 			TEXT,
	
	isactive 			BOOLEAN NOT NULL DEFAULT false,
	created 			TIMESTAMP NOT NULL DEFAULT NOW(),
	createdby 			INTEGER NOT NULL DEFAULT 0,
	updated 			TIMESTAMP NOT NULL DEFAULT NOW(),
	updatedby 			INTEGER NOT NULL DEFAULT 0,
	description 			TEXT 
);

------------------trigger-----------------------------
CREATE TRIGGER tri_updated before UPDATE
ON  patient_vitals FOR EACH ROW EXECUTE PROCEDURE
pro_updated();





details:
	patient_ids
	allergies
	addresses
	relations
		family_history
	encounter
	orders
	lab
	pharmacy
	
--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

------functions 
------stored procedures


CREATE OR REPLACE FUNCTION proc_getserial(IN serial_name character varying, OUT serial_value integer) RETURNS integer AS $$
DECLARE
    procInc integer; 
    procminVal integer; 
    procMaxVal integer; 
    procStartVal integer; 
    procCurrVal integer;
    procFormat_str character varying;
BEGIN
  select increment, min_value, max_value, start_value, curr_value, format_str INTO procInc, procminVal, procMaxVal, procStartVal, procCurrVal, procFormat_str from hr_getSerial where name=serial_name;
   
    IF NOT FOUND THEN
       RAISE EXCEPTION 'serial % not found', serial_name;
    END IF;
   
END;
$$ LANGUAGE plpgsql;
  




CREATE OR REPLACE FUNCTION proc_addSerial(procName character varying, procFormat_str character varying, out procSuccess integer) RETURNS integer AS $$
DECLARE
    procName character varying;
    procTest character varying;
    procFormat_str character varying;
    procMaxId integer;
    procInc integer;
BEGIN

   procSuccess := -1;
   procMaxId := NULL;

   select into procTest name from hr_getserial where name=procName ;    
    IF FOUND THEN
           RAISE EXCEPTION 'SERIAL NAME  % ALLREADY EXISTS MUST BE UNIQUE', procName; 
           return;
    END IF;


   select max(getserial_id) into procMaxId from hr_getserial; 
   IF NOT FOUND THEN
     procMaxId := 1;       
    END IF;

    
  
 
   BEGIN
    insert into hr_getSerial (getserial_id, name, format_str) values (procMaxId, procName, procFormat_str);
     IF FOUND THEN
     procSuccess := 0;
     return;
     END IF;

   END; 

END;
$$ LANGUAGE plpgsql;
  


---/////////////////////////////////////////////////////////////////////////////////////////////////////////////

CREATE FUNCTION lower_case_columns(_schemaname text) RETURNS void AS
$$
DECLARE
  colname text;
  tablename text;
  sql text;
BEGIN
  FOR tablename,colname in select table_name,column_name FROM information_schema.columns 
    WHERE table_schema=_schemaname AND column_name<>lower(column_name)
  LOOP
    sql:=format('ALTER TABLE %I.%I RENAME COLUMN %I TO %I',
       _schemaname,
       tablename,
       colname,
       lower(colname));
    raise notice '%', sql;
    execute sql;
  END LOOP;
END;
$$ LANGUAGE plpgsql;
