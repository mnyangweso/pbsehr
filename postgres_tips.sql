-----------------------------------------------------------------------------------------------------------
--return the strings before the = signs
-----------------------------------------------------------------------------------------------------------

select LEFT(firstregimen, strpos(firstregimen,'=')-1) FROM tlkregimenfirstx;

-----------------------------------------------------------------------------------------------------------
--remove letters from a column
-----------------------------------------------------------------------------------------------------------

column_name = regexp_replace(column_name, '[^0-9\.]', '', 'g');

-----------------------------------------------------------------------------------------------------------
-------------------------------------------------------server uptime---------------------------------------
-----------------------------------------------------------------------------------------------------------

SELECT date_trunc('second',current_timestamp - pg_postmaster_start_time()) as uptime;

-----------------------------------------------------------------------------------------------------------
--finding the size of database
-----------------------------------------------------------------------------------------------------------

SELECT pg_database_size(current_database()); --current
SELECT sum(pg_database_size(datname)) FROM pg_database; --all databases
select pg_relation_size('accounts'); --size of a table

-----------------------------------------------------------------------------------------------------------
------select size largest tables
--The following basic query will tell us the "Top 10 Biggest Tables":
-----------------------------------------------------------------------------------------------------------

SELECT table_name,pg_relation_size(table_name) as size
FROM information_schema.tables
WHERE table_schema NOT IN ('information_schema','pg_catalog')
ORDER BY size DESC
LIMIT 10;

-----------------------------------------------------------------------------------------------------------
--- finding unused indexrelname
-----------------------------------------------------------------------------------------------------------

SELECT schemaname, relname, indexrelname, idx_scan FROM pg_stat_user_indexes ORDER BY idx_scan;

-----------------------------------------------------------------------------------------------------------
--Getting N unique most similar values with usage KNNsearch and pg_tgrm module
-----------------------------------------------------------------------------------------------------------
SELECT DISTINCT ON (village_name <-> 'Benešov') village_name, village_name<->'Benešov' 
FROM villages 
ORDER BY village_name <-> 'Benešov' LIMIT 10;


CREATE DATABASE cpad WITH TEMPLATE ccc OWNER postgres;

ALTER database template1 SET datestyle TO 'SQL, DMY';

SELECT table_name FROM information_schema.columns WHERE column_name ~* 'combination';
SELECT table_name, column_name, column_default FROM information_schema.columns WHERE table_name='tblvisit_information';
SELECT table_schema,table_name FROM information_schema.tables ORDER BY table_schema,table_name;

SELECT table_schema,table_name,table_type FROM information_schema.tables where table_schema='public'and table_type='BASE TABLE' ORDER BY table_schema,table_name;

SELECT table_name FROM information_schema.tables where table_schema='public'and table_type='BASE TABLE' ORDER BY table_name;
 
 
CREATE FUNCTION lower_case_columns(_schemaname text) RETURNS void AS
$$
DECLARE
  colname text;
  tablename text;
  sql text;
BEGIN
  FOR tablename,colname in select table_name,column_name FROM information_schema.columns 
    WHERE table_schema=_schemaname AND column_name<>lower(column_name)
  LOOP
    sql:=format('ALTER TABLE %I.%I RENAME COLUMN %I TO %I',
       _schemaname,
       tablename,
       colname,
       lower(colname));
    raise notice '%', sql;
    execute sql;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

-----------------------------------------------------------------------------------------------------------
--   Finding the largest databases in your cluster
-----------------------------------------------------------------------------------------------------------

SELECT d.datname AS Name,  pg_catalog.pg_get_userbyid(d.datdba) AS Owner,
    CASE WHEN pg_catalog.has_database_privilege(d.datname, 'CONNECT')
        THEN pg_catalog.pg_size_pretty(pg_catalog.pg_database_size(d.datname))
        ELSE 'No Access'
    END AS SIZE
FROM pg_catalog.pg_database d
    ORDER BY
    CASE WHEN pg_catalog.has_database_privilege(d.datname, 'CONNECT')
        THEN pg_catalog.pg_database_size(d.datname)
        ELSE NULL
    END DESC -- nulls first
    LIMIT 20;

--SELECT pg_database.datname, pg_size_pretty(pg_database_size(pg_database.datname)) AS size FROM pg_database;

-----------------------------------------------------------------------------------------------------------
--   Finding the size of your biggest relations
-----------------------------------------------------------------------------------------------------------

SELECT nspname || '.' || relname AS "relation",
    pg_size_pretty(pg_relation_size(C.oid)) AS "size"
  FROM pg_class C
  LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
  WHERE nspname NOT IN ('pg_catalog', 'information_schema')
  ORDER BY pg_relation_size(C.oid) DESC
  LIMIT 20;


-----------------------------------------------------------------------------------------------------------
--   Finding the total size of your biggest tables
-----------------------------------------------------------------------------------------------------------



SELECT nspname || '.' || relname AS "relation",
    pg_size_pretty(pg_total_relation_size(C.oid)) AS "total_size"
  FROM pg_class C
  LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
  WHERE nspname NOT IN ('pg_catalog', 'information_schema')
    AND C.relkind <> 'i'
    AND nspname !~ '^pg_toast'
  ORDER BY pg_total_relation_size(C.oid) DESC
  LIMIT 20;


-----------------------------------------------------------------------------------------------------------
--   Generate DROP TABLE for a given prefix
-----------------------------------------------------------------------------------------------------------
SELECT 'DROP TABLE ' || tablename || ';' FROM pg_tables WHERE tablename LIKE 'prefix%' AND schemaname = 'public';

